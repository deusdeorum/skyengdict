<?php
namespace backend\controllers;

use app\models\Mistake;
use Yii;
use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\filters\ContentNegotiator;

/**
 * Mistake controller
 */
class MistakeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['get'],
                ],
            ],
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ]);
    }


    public function actionIndex()
    {

        $mistakes = Mistake::find()
            ->select(['_id' => false])
            ->orderBy(['count' => SORT_DESC])
            ->limit(10)
            ->all();

        return $mistakes;
    }
}
