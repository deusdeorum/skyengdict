<?php
namespace backend\controllers;

use app\models\Test;
use Yii;
use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\filters\ContentNegotiator;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\AccessControl;

/**
 * Test controller
 */
class TestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'results' => ['get'],
                    'login' => ['post'],
                    'task' => ['post'],
                    'answer' => ['post']
                ],
            ],
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'authenticator' => [
                'class' => HttpBearerAuth::className(),
                'only' => ['task', 'answer'],
                //'only' => [''],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['task', 'answer'],
                'rules' => [
                    [
                        'actions' => ['task', 'answer'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]

        ]);
    }

    public function actionLogin()
    {
        $model = new Test();
        $model->name = Yii::$app->request->post('name');

        if ($model->validate()) {
            // generating token
            $model->generateAuthKey();
            $model->generateTask();
            $model->save();

            Yii::$app->user->login($model);

            return ['token' => Yii::$app->user->identity->getAuthKey()];
        }
        else {
            return $model->errors;
        }
    }

    public function actionTask()
    {

        $testModel = Yii::$app->user->identity;
        if (!$testModel->finished)
            return $testModel->getTask();

    }


    public function actionAnswer()
    {

        $testModel = Yii::$app->user->identity;

        if ($testModel->finished)
            return;

        $json = $testModel->checkAnswer(Yii::$app->request->post('_id'));

        return $json;

    }

    public function actionResults()
    {

        $results = Test::find()
            ->select(['_id' => false, 'tasks' => false, 'token' => false])
            ->where(['correct' => ['$gt' => 0]])
            ->orderBy(['correct' => SORT_DESC])
            ->limit(10)
            ->all();

        return $results;
    }
}
